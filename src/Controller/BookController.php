<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Author;
use App\Entity\BookCategory;
use App\Form\BookType;
use App\Form\DeleteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Book;

/**
 * @Route("/books")
 */
class BookController extends AbstractController
{
    /**
     * @Route("/new", methods={"GET", "POST"}, name="app_book_new", condition="request.isXmlHttpRequest()")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function newBook(Request $request): Response
    {
        $book = new Book();

        $form = $this->createForm(BookType::class, $book,
            ['action' => $this->generateUrl($request->get('_route'))]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            $file = $book->getImage();
            if($file) {
                $filename = 'book_' . uniqid('', false) . '.' . $file->guessExtension();
                $file->move($this->getParameter('upload_directory_books'), $filename);
                $book->setImage($this->getParameter('upload_directory_books') . $filename);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($book);
            $em->flush();
            $this->addFlash('success', 'Le livre a bien été ajouté');
            return new Response('success');
        }

        return $this->render('front/popup_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id<\d+>}/edit", methods={"GET", "POST"}, name="app_book_edit", condition="request.isXmlHttpRequest()")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function editBook(Request $request, Book $book): Response
    {
        //on récup la path de l'image en base
        $img=$book->getImage();

        $form = $this->createForm(BookType::class, $book,
            ['action' => $this->generateUrl($request->get('_route'),['id' => $book->getId()])]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            //si image post
            if ($book->getImage() !== null) {
                $file = $book->getImage();
                $filename = 'book_' . uniqid('', false) . '.' . $file->guessExtension();
                $file->move($this->getParameter('upload_directory_books'), $filename);
                $book->setImage($this->getParameter('upload_directory_books') . $filename);
                if ($img !== null) {
                    if (file_exists($img)) {
                        unlink($img);
                    }
                }
            } else {
                $book->setImage($img);
            }

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash('success', 'Le livre a bien été modifié');
            return new Response('success');
        }

        return $this->render('front/popup_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id<\d+>}/delete", methods={"GET","POST"}, name="app_book_delete", condition="request.isXmlHttpRequest()")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function deleteBook(Request $request, Book $book): Response
    {
        $form = $this->createForm(DeleteType::class, $book,
            ['action' => $this->generateUrl($request->get('_route'),['id' => $book->getId()])]
        );
        $form->handleRequest($request);

        if ($book->getImage() === null) {
            $book->setImage('empty');
        }
        $file = new File($book->getImage(), false);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            if (file_exists($file->getPath())) {
                unlink($book->getImage());
            }
            $em = $this->getDoctrine()->getManager();
            $em->remove($book);
            $em->flush();
            $this->addFlash('success', 'Le livre a bien été supprimé');
            return new Response('success');
        }
        return $this->render('front/popup_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/manage", methods={"GET", "POST"}, name="app_book_manage")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function manage(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $books = $em->getRepository(Book::class)->findAll();
        $categorys = $em->getRepository(BookCategory::class)->findAll();
        $authors = $em->getRepository(Author::class)->findAll();
        return $this->render('front/book_manage.html.twig', [
            'books' => $books,
            'categorys' => $categorys,
            'authors' => $authors,
        ]);
    }

    /**
     * @Route("/", methods={"GET"}, name="app_book_all")
     */
    public function showBook(): Response
    {
        $books = $this->getDoctrine()->getManager()->getRepository(Book::class)->findAll();
        return $this->render('front/books.html.twig', [
            'books' => $books,
        ]);
    }
}