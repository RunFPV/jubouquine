<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\ArticleType;
use App\Form\DeleteType;
use App\Repository\ArticleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/articles")
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/scroll/{id}", methods={"GET", "POST"}, name="app_article_scroll", condition="request.isXmlHttpRequest()")
     */
    public function scrollArticle(Request $request, $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository(Article::class)->findLastArticlesFirstMaxBefore(0,5, $id);
        return $this->render('front/article_liste.html.twig', [
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/{id<\d+>}", methods={"GET", "POST"}, name="app_article_show")
     */
    public function showArticle(Article $article): Response
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->findOneBy(['id' => $article->getId()]);

        $response = $this->forward('App\Controller\CommentController::newComment', [
            'article'  => $article,
            'comment' => new Comment()
        ]);

        return $response;

        /*return $this->render('front/article_template.html.twig', [
            'article' => $article,
        ]);*/
    }

    /**
     * @Route("/Like/{id<\d+>}", methods={"GET", "POST"}, name="app_article_get_like", condition="request.isXmlHttpRequest()")
     */
    public function getLikeArticle(Article $article): Response
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->findOneBy(['id' => $article->getId()]);
        return new Response($article->getUser()->count());
    }

    /**
     * @Route("/new", methods={"GET", "POST"}, name="app_article_new", condition="request.isXmlHttpRequest()")
     * @Security("is_granted('ROLE_ADMIN')")
     * @throws \Exception
     */
    public function newArticle(Request $request): Response
    {
        $article = new Article();

        $form = $this->createForm(ArticleType::class, $article,
            ['action' => $this->generateUrl($request->get('_route'))]
        );
        $form->handleRequest($request);
        $article->setDate(new \DateTime('now',timezone_open('Europe/Paris')));

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            $file = $article->getImage();
            if($file) {
                $filename = 'article_' . uniqid('', false) . '.' . $file->guessExtension();
                $file->move($this->getParameter('upload_directory_articles'), $filename);
                $article->setImage($this->getParameter('upload_directory_articles') . $filename);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();
            $this->addFlash('success', 'L\'article a bien été ajouté');
            return new Response('success');
        }

        return $this->render('front/popup_new_article.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id<\d+>}/edit", methods={"GET", "POST"}, name="app_article_edit", condition="request.isXmlHttpRequest()")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function editArticle(Request $request, Article $article): Response
    {
        //on récup la path de l'image en base
        $img=$article->getImage();

        $form = $this->createForm(ArticleType::class, $article,
            ['action' => $this->generateUrl($request->get('_route'),['id' => $article->getId()])]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            //si image post
            if ($article->getImage() !== null) {
                $file = $article->getImage();
                $filename = 'article_' . uniqid('', false) . '.' . $file->guessExtension();
                $file->move($this->getParameter('upload_directory_articles'), $filename);
                $article->setImage($this->getParameter('upload_directory_articles') . $filename);
                if ($img !== null) {
                    if (file_exists($img)) {
                        unlink($img);
                    }
                }
            } else {
                $article->setImage($img);
            }
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash('success', 'L\'article a bien été modifié');
            return new Response('success');
        }

        return $this->render('front/popup_new_article.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id<\d+>}/delete", methods={"GET","POST"}, name="app_article_delete", condition="request.isXmlHttpRequest()")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function deleteArticle(Request $request, Article $article): Response
    {
        $form = $this->createForm(DeleteType::class, $article,
            ['action' => $this->generateUrl($request->get('_route'),['id' => $article->getId()])]
        );
        $form->handleRequest($request);

        if ($article->getImage() === null) {
            $article->setImage('empty');
        }
        $file = new File($article->getImage(), false);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            if (file_exists($file->getPath())) {
                unlink($article->getImage());
            }
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();
            $this->addFlash('success', 'L\'article a bien été supprimé');
            return new Response('success');
        }
        return $this->render('front/popup_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id<\d+>}/islike", methods={"GET","POST"}, name="app_article_is_like", condition="request.isXmlHttpRequest()")
     * @Security("is_granted('ROLE_USER')")
     */
    public function isLikeArticle(Article $article): Response
    {
        if(in_array($this->getUser(),$article->getUser()->toArray())) {
            return new Response('Liked');
        }
        return new Response('notLiked');
    }

    /**
     * @Route("/{id<\d+>}/like", methods={"GET","POST"}, name="app_article_like", condition="request.isXmlHttpRequest()")
     * @Security("is_granted('ROLE_USER')")
     */
    public function likeArticle(Request $request, Article $article): Response
    {
        $req = $request->get('req');
        //si l'utilisateur veut aimer
        if (strpos($req, 'primary') !== false) {
            //si l'user n'a pas encore aimé
            if(!in_array($this->getUser(),$article->getUser()->toArray())) {
                $article->addUser($this->getUser());
            }
        } else {
            //si l'user a aimé
            if(in_array($this->getUser(),$article->getUser()->toArray())) {
                $article->removeUser($this->getUser());
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();
        return new Response('Done');
    }
}