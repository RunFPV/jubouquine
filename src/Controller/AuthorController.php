<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Author;
use App\Form\AuthorType;
use App\Form\DeleteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/author")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class AuthorController extends AbstractController
{
    /**
     * @Route("/new", methods={"GET", "POST"}, name="app_author_new", condition="request.isXmlHttpRequest()")
     */
    public function newAuthor(Request $request): Response
    {
        $author = new Author();

        $form = $this->createForm(AuthorType::class, $author,
            ['action' => $this->generateUrl($request->get('_route'))]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            $em = $this->getDoctrine()->getManager();
            $authorBDD = $em->getRepository(Author::class)->findOneBy(['name' => $author->getName()]);

            if (!$authorBDD) {
                $this->getDoctrine()->getManager()->persist($author);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', 'L\'auteur a bien été ajouté');
                return new Response('success');
            } else {
                $this->addFlash('danger', 'Impossible d\'ajouter l\'auteur car il existe déjà');
                return new Response('danger');
            }
        }

        return $this->render('front/popup_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id<\d+>}/edit", methods={"GET", "POST"}, name="app_author_edit", condition="request.isXmlHttpRequest()")
     */
    public function editAuthor(Author $author, Request $request): Response
    {
        $form = $this->createForm(AuthorType::class, $author,
            ['action' => $this->generateUrl($request->get('_route'),['id' => $author->getId()])]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'L\'auteur a bien été modifié');
            return new Response('success');
        }

        return $this->render('front/popup_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id<\d+>}/delete", methods={"GET","POST"}, name="app_author_delete", condition="request.isXmlHttpRequest()")
     */
    public function deleteAuthor(Request $request, Author $author): Response
    {
        $form = $this->createForm(DeleteType::class, $author,
            ['action' => $this->generateUrl($request->get('_route'),['id' => $author->getId()])]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($author);
            $em->flush();
            $this->addFlash('success', 'L\'auteur a bien été supprimé');
            return new Response('success');
        }
        return $this->render('front/popup_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}