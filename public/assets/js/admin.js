function btnCRUD(path) {
    var title;
    if (path.includes('author')) {
        title = 'Auteur';
    } else if (path.includes('category')) {
        title = 'Catégorie';
    } else if (path.includes('book')) {
        title = 'Livre';
    } else if (path.includes('article')) {
        title = 'Article';
    } else if (path.includes('user')) {
        title = 'Utilisateur';
    }
    $.ajax(path, {
        success: function (data) {
            $('#modal').find('#modalTitle').html(title);
            $('#modal').find('#modalBody').html(data);
            $('#modal').modal('show');
        }
    });
}

$(document).on('submit', 'form', function (e) {
    e.preventDefault();

    $form = $(e.target);

    //petite animation sur le bouton submit, pas piquer des annetons
    var $submitButton = $form.find(':submit');
    var $buttonVal = $submitButton.html();
    $submitButton.html('<i class="fa fa-spinner fa-pulse"></i>');
    $submitButton.prop('disabled', true);

    $form.ajaxSubmit({
        type: 'post',
        success: function (data) {
            if (data === 'success' || data === 'danger') {
                location.reload();
            } else {
                $('#modalBody').html(data);
                $submitButton.html($buttonVal);
                $submitButton.prop('disabled', false);
            }
        },
        error: function (jqXHR, status, error) {
            $submitButton.html($buttonVal);
            $submitButton.prop('disabled', false);
        }
    });
});